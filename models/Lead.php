<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $notes
 * @property string $status
 * @property string $owner
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'notes', 'status', 'owner', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['name', 'email', 'phone', 'notes', 'status', 'owner', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255]
        ];
    }

    
    
    public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

    
    
    
    
    public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
    /**
     * Defenition of relation to status table
     */  
 	
    
 
	public function getStatusItem()
   {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
    
    
    
    public static function getUsers()
    {
    	$allUsers = self::find()->all();
    	$usersFirstname = ArrayHelper::
    		map($allUsers, 'id', 'firstname');
    	$usersLastname = ArrayHelper::
    		map($allUsers, 'id', 'lastname');
    	$users=[];
    	foreach($usersFirstname as $id=>$firstname)
    	{
    		$userd[id]=$firstname. ' ' . $usersLastname[$id];
    	}
    	return $users;
    }
    
    
    public static function getStatuses()
    {
    	$allStatuses = self::find()->all();
    	$allStatusesArray = ArrayHelper::
    		map($allStatuses, 'id', 'name');
    	return $allStatusesArray;
    }
    
    
    
    
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'notes' => 'Notes',
            'status' => 'Status',
            'owner' => 'Owner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    
    
    
    
    
    
    
    
    
}
